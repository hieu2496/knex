export const typeDef = `
    extend type Query {
        getAllPersons: [Persons]
        findPerson(id: ID!): Persons
    }

    extend type Mutation {
        createPersons(input: PersonsInput): Persons
        updatePersons(id: ID!, input: PersonsInput): Persons

    }

    input PersonsInput {
        name: String,
        age: String,
        gender: String,
    }

    type Persons {
        id: ID,
        name: String,
        age: String,
        gender: String,
    }
`;
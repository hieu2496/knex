import { PersonsModel } from '../../models/persons';

export const resolver = {
    Query: {
        getAllPersons: () => PersonsModel.getAllPersons(),
        findPerson(root, { id }) {
            return PersonsModel.findPerson(id);
        }
    },
    Mutation: {
        createPersons(root, { input }) {
            return PersonsModel.createPersons(input);
        },
        // deletePersons(root, { id }) {
        //     return PersonsModel.deletePersons(id);
        // },
        updatePersons(root, { id, input }) {
            if (id) {
                return PersonsModel.updatePersons(id, input);
            }
            return false;
        },
        // insertPersons(root, { input }) {
        //     return PersonsModel.insertPersons(input);
        // }
    }
}

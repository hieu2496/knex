import { knex } from '../connectors';
import * as promise from 'bluebird';

class Persons {
    getAllPersons() {
        return knex('persons').select();
    }
    findPerson(id: number) {
        return knex('persons').where('id', id).first();

    }

    createPersons(input: object) {
        if (input) {
            return knex('persons').insert(input).then(function(result) {
                return knex('persons').where('id', result[0]).first();
            });
        }
        return false;
    }

    // deletePersons(id: number) {
    //     if (id) {
    //         return knex('persons').where('id', id).del();
    //     }
    //     return false;
    // }

    updatePersons(id: number, input: object) {
        if (id) {
            return knex('persons').where('id', id).update(input);
        }
        return false;
    }

    // insertPersons(persons: object[]) {
    //     return knex.transaction((trans) => {
    //         promise.map(persons, (person) => {
    //             return knex.insert(person).into('persons').transacting(trans);
    //         })
    //         .then(trans.commit)
    //         .catch(trans.rollback);
    //       })
    //       .then(function(inserts) {
    //         console.log(inserts.length);
    //       })
    //       .catch(function(error) {
    //         console.log("rollback db");
    //         console.error(error);
    //         return false;
    //       });
    // }
}


export const PersonsModel = new Persons();
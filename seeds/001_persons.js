
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('persons').del()
    .then(function () {
      // Inserts seed entries
      return knex('persons').insert([
       { name: "hieu", age: "auser", gender: "male"}
      ]);
    });
};

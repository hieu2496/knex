
exports.up = function(knex, Promise) {
    return knex.schema.createTable('persons', function (table) {
        table.increments('id');
        table.string('name');
        table.string('age');
        table.string('gender');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('persons');
};
